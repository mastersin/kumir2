Build manual for Ubuntu 14.04 LTS
---------------------------------

1. Required additional packages to install before build:
    - cmake
    - g++
    - libqt4-dev
    - libqca2-dev
    - zlib1g-dev
    - libboost-dev
    - llvm-dev
    - clang
    
2. Build and install commands
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ../
    sudo make install # installs to /usr/local, requires root password
    
    

    