<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>CoreGUI::AboutDialog</name>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="14"/>
        <source>About Kumir</source>
        <translation>О системе &quot;Кумир&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="24"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="34"/>
        <source>Licensee</source>
        <translation>Права на использование</translation>
    </message>
    <message utf8="true">
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="40"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic;&quot;&gt;За пределами Российской Федерации данная программа может свободно распространяться по лиценции GNU GPL v2.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt; font-style:italic;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt; font-style:italic;&quot;&gt;На территории Российской Федерации данная программа распространяется в соответствии с данным Лицензионным соглашением.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;ЛИЦЕНЗИОННОЕ СОГЛАШЕНИЕ&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;1. Устанавливая данный программный продукт, Вы автоматически принимаете условия данного лицензионного соглашения.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;2. Вы можете устанавливать данную программу на любое число компьютеров неограниченное число раз.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;3. Вы можете делать  неограниченное число копий данного программного продукта.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;4. Вы можете передавать копии данного программного продукта возмездно или безвозмездно неограниченному числу третьих лиц.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;5. Вы имеете право загрузить с сайта разработчика исходные тексты данного программного продукта и использовать их в соответствии с требованиями лицензии GNU GPL v2, текст которой на английском языке прилагается в файле license.gpl.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;6. Разработчик не дает никаких гарантий работоспобосности данного продукта и не несет ответственности за любой ущерб, причиненный вследствии установки или запуска данного программного продукта.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="89"/>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="109"/>
        <source>unknown</source>
        <translation>неизвестно</translation>
    </message>
    <message>
        <source>GIT hash:</source>
        <translation type="obsolete">GIT отпечаток:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="99"/>
        <source>Last modified:</source>
        <translation>Последнее изменение:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="146"/>
        <source>Copy system information to clipboard</source>
        <translation>Скопировать информацию о системе в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="166"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="169"/>
        <source>Esc</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="79"/>
        <source>Version:</source>
        <translation>Версия:</translation>
    </message>
    <message>
        <source>Kumir version: %1 (revision: %2)</source>
        <translation type="obsolete">Версия %1 (SVN-ревизия №%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="126"/>
        <source>Parameter</source>
        <translation>Параметр</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="70"/>
        <source>System Information</source>
        <translation>Информация об окружении</translation>
    </message>
    <message>
        <source>Qt Version:</source>
        <translation type="obsolete">Версия Qt</translation>
    </message>
    <message>
        <source>Operating system:</source>
        <translation type="obsolete">Операционная система</translation>
    </message>
    <message>
        <source>Launched binary:</source>
        <translation type="obsolete">Исполняемый файл:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.ui" line="131"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="61"/>
        <source>Copied to clipboard</source>
        <translation>Скопировано в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="62"/>
        <source>&lt;b&gt;The following text has been copied to clipboard:&lt;/b&gt;

%1</source>
        <translation>&lt;b&gt;Следующий текст был скопирован в буфер обмена:&lt;/b&gt;

%1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="91"/>
        <source>Qt Version</source>
        <translation>Версия Qt</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="98"/>
        <source>Operating System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="143"/>
        <source>Execuable Path</source>
        <translation>Выполняемый файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="156"/>
        <source>Loaded Modules</source>
        <translation>Модули системы</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/aboutdialog.cpp" line="170"/>
        <source>Settings Files</source>
        <translation>Файлы настроек</translation>
    </message>
</context>
<context>
    <name>CoreGUI::ConfirmCloseDialod</name>
    <message>
        <source>Confirm quit</source>
        <translation type="obsolete">Подвтерждение выхода</translation>
    </message>
    <message>
        <source>Do one of the following on quit:</source>
        <translation type="obsolete">Перед выходом из Кумир:</translation>
    </message>
    <message>
        <source>Do not save files, but save state to be restored on next launch</source>
        <translation type="obsolete">Сохранить состояние редактирования, но не сохранять файлы</translation>
    </message>
    <message>
        <source>Save all opened files and close them</source>
        <translation type="obsolete">Сохранить и закрыть все открытые файлы</translation>
    </message>
    <message>
        <source>Save unsaved file</source>
        <translation type="obsolete">Сохранить файл</translation>
    </message>
    <message>
        <source>Save nothing and quit</source>
        <translation type="obsolete">Выйти без сохранения</translation>
    </message>
    <message>
        <source>Cancel quit</source>
        <translation type="obsolete">Отменить выход</translation>
    </message>
</context>
<context>
    <name>CoreGUI::DebuggerView</name>
    <message>
        <location filename="../../../src/plugins/coregui/debuggerview.cpp" line="60"/>
        <source>Current values available only while running program in step-by-step mode</source>
        <translation>Текущие значения величин отображаются при выполнении по шагам</translation>
    </message>
</context>
<context>
    <name>CoreGUI::DebuggerWindow</name>
    <message>
        <source>Main algorithm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorithm &apos;%1&apos;</source>
        <translation type="obsolete">Алгоритм &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Globals</source>
        <translation type="obsolete">Глобальные величины</translation>
    </message>
    <message>
        <source>Globals of &apos;%1&apos;</source>
        <translation type="obsolete">Глобальные в исполнителе &quot;%1&quot; </translation>
    </message>
    <message>
        <source>This variable is a reference.
Right click to navigate target</source>
        <translation type="obsolete">Эта величина является ссылкой. Для перехода по ссылке нажмите правую кнопку мыши</translation>
    </message>
    <message>
        <source>Current values available only while running program in step-by-step mode</source>
        <translation type="obsolete">Текущие значения величин отображаются при выполнении по шагам</translation>
    </message>
</context>
<context>
    <name>CoreGUI::GUISettingsPage</name>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="14"/>
        <source>User Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="20"/>
        <source>Docking layout</source>
        <translation>Расположение дополнительных окон</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="41"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="48"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="55"/>
        <source>Rows first</source>
        <translation>Исполнители 
в одной строке 
с окном ввода-вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="62"/>
        <source>Columns first</source>
        <translation>Исполнители 
в одном столбце 
с Практикумом</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.ui" line="85"/>
        <source>Visible icons in toolbar</source>
        <translation>Отображаемые значки над редактором</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="83"/>
        <source>Cut selection to clipboard</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="89"/>
        <source>Copy selection to clipboard</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="95"/>
        <source>Paste from clipboard</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="111"/>
        <source>Undo last action</source>
        <translation>Отменить последнее действие</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="117"/>
        <source>Redo last undoed action</source>
        <translation>Повторить последнее действие</translation>
    </message>
</context>
<context>
    <name>CoreGUI::IOSettingsEditorPage</name>
    <message>
        <location filename="../../../src/plugins/coregui/iosettingseditorpage.ui" line="14"/>
        <source>Input/Output</source>
        <translation>Ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/iosettingseditorpage.ui" line="20"/>
        <source>Terminal width</source>
        <translation>Ширина области ввода-вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/iosettingseditorpage.ui" line="26"/>
        <source>Use fixed width for new launch sessions</source>
        <translation>Ограничить ширину вывода для последующих запусков программ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/iosettingseditorpage.ui" line="38"/>
        <source>Width in characters:</source>
        <translation>Ширина в символах:</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirProgram</name>
    <message>
        <source>Fast run</source>
        <translation type="obsolete">Ускоренное выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="139"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="61"/>
        <source>Regular run</source>
        <translation>Обычное выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="75"/>
        <source>Testing run</source>
        <translation>Запустить тестирование</translation>
    </message>
    <message>
        <source>Step run</source>
        <translation type="obsolete">Пошаговое выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="167"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="92"/>
        <source>Step in</source>
        <translation>шаг</translation>
    </message>
    <message>
        <source>Step out</source>
        <translation type="obsolete">Выход из алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="145"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="124"/>
        <source>Stop</source>
        <translation>Остановить выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="133"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="147"/>
        <source>Blind run</source>
        <translation>Без показа на полях</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="299"/>
        <source>This program does not have testing algorithm</source>
        <translation>У этой программы нет тестирующего алгоритма</translation>
    </message>
    <message>
        <source>This program does not have testing algorhitm</source>
        <translation type="obsolete">Программа не содержит тестирующего алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="90"/>
        <source>Do big step</source>
        <translation>ШАГ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="102"/>
        <source>Do small step</source>
        <translation>шаг</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="117"/>
        <source>Run to cursor</source>
        <translation>Выполнить до курсора</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="119"/>
        <source>Run until the line editor cursor in</source>
        <translation>Выполнить программу до строки, в которой находится курсор редактирования</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="173"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="104"/>
        <source>Step to end</source>
        <translation>До конца алгоритма</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="114"/>
        <source>Run to end of algorhitm</source>
        <translation>До конца алгоритма</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="obsolete">Нераспознанная ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="488"/>
        <source>Evaluation error</source>
        <translation>Ошибка выполнения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="497"/>
        <source>Evaluation finished</source>
        <translation>Выполнение завершено</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="479"/>
        <source>Evaluation terminated</source>
        <translation>Выполнение прервано</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="161"/>
        <location filename="../../../src/plugins/coregui/kumirprogram.cpp" line="80"/>
        <source>Step over</source>
        <translation>ШАГ</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirVariablesWebObject</name>
    <message>
        <source>User algorhitms and variables</source>
        <translation type="obsolete">Алгоритмы и величины</translation>
    </message>
    <message>
        <source>Global variables</source>
        <translation type="obsolete">Глобальные величины</translation>
    </message>
    <message>
        <source>Main algorhitm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorhitm &quot;%1&quot;</source>
        <translation type="obsolete">Алгоритм «%1»</translation>
    </message>
    <message>
        <source>User algorithm and variables</source>
        <translation type="obsolete">Алгоритмы и величины</translation>
    </message>
    <message>
        <source>Main algorithm</source>
        <translation type="obsolete">Главный алгоритм</translation>
    </message>
    <message>
        <source>Algorithm &quot;%1&quot;</source>
        <translation type="obsolete">Алгоритм &quot;%1&quot;</translation>
    </message>
    <message>
        <source>integer</source>
        <translation type="obsolete">цел</translation>
    </message>
    <message>
        <source>real</source>
        <translation type="obsolete">вещ</translation>
    </message>
    <message>
        <source>charect</source>
        <translation type="obsolete">сим</translation>
    </message>
    <message>
        <source>string</source>
        <translation type="obsolete">лит</translation>
    </message>
    <message>
        <source>boolean</source>
        <translation type="obsolete">лог</translation>
    </message>
    <message>
        <source>table</source>
        <translation type="obsolete">таб</translation>
    </message>
    <message>
        <source>undefined</source>
        <translation type="obsolete">не определено</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Тип</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Значение</translation>
    </message>
    <message>
        <source>Global table &quot;%1&quot;</source>
        <translation type="obsolete">Глобальная таблица «%1»</translation>
    </message>
    <message>
        <source>Table &quot;%1&quot; from main algorhitm</source>
        <translation type="obsolete">Таблица «%1» главного алгоритма</translation>
    </message>
    <message>
        <source>Table &quot;%1&quot; from algorhitm &quot;%2&quot;</source>
        <translation type="obsolete">Таблица «%1» алгоритма «%2»</translation>
    </message>
    <message>
        <source> (module &quot;%1&quot;)</source>
        <translation type="obsolete"> (испонитель «%1»)</translation>
    </message>
    <message>
        <source>Unknown table</source>
        <translation type="obsolete">Неизвестная таблица</translation>
    </message>
    <message>
        <source>The table is not initialized yet</source>
        <translation type="obsolete">Таблица пока не определена</translation>
    </message>
    <message>
        <source>Yout must run program as regular or step run to show table values</source>
        <translation type="obsolete">Запустите программу в обычном или пошаговом режиме</translation>
    </message>
</context>
<context>
    <name>CoreGUI::KumirVariablesWindow</name>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">Тип</translation>
    </message>
    <message>
        <source>Value</source>
        <translation type="obsolete">Значение</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="obsolete">?</translation>
    </message>
</context>
<context>
    <name>CoreGUI::MainWindow</name>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="14"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="953"/>
        <source>Kumir</source>
        <translation>Кумир</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="56"/>
        <source>File</source>
        <translation>Программа</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="obsolete">Создать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="83"/>
        <source>Help</source>
        <translation>Инфо</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="91"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="96"/>
        <source>Insert</source>
        <translation>Вставка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="101"/>
        <source>Run</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="106"/>
        <source>Window</source>
        <translation>Окна</translation>
    </message>
    <message>
        <source>Program</source>
        <translation type="obsolete">Программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="122"/>
        <source>Ctrl+N</source>
        <translation></translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="obsolete">Текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="119"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="55"/>
        <source>New program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="127"/>
        <source>New text</source>
        <translation>Новый текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="130"/>
        <source>Ctrl+Shift+N</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="135"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="61"/>
        <source>Open...</source>
        <translation>Загрузить...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="138"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="143"/>
        <source>Recent files</source>
        <translation>Недавние файлы</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="148"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="67"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1367"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1858"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1986"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2175"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="151"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="156"/>
        <source>Save as...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="161"/>
        <source>Save all</source>
        <translation>Сохранить во всех вкладках</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="164"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="169"/>
        <source>Close</source>
        <translation>Закрыть вкладку</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="172"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="177"/>
        <source>Switch workspace...</source>
        <translation>Выбрать рабочий каталог...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="182"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="185"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="196"/>
        <source>Manuals</source>
        <translation>Справочные руководства</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="236"/>
        <source>Variable Current Values</source>
        <translation>Текущие значения величин</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="250"/>
        <source>Show Console Pane</source>
        <translation>Отображать область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="253"/>
        <source>F12</source>
        <translation>F12</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="258"/>
        <source>Make native executable...</source>
        <translation>Сохранить как выполняемый файл...</translation>
    </message>
    <message>
        <source>Variables</source>
        <translation type="obsolete">Величины</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="239"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <source>Usage...</source>
        <translation type="obsolete">Руководство пользователя...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="199"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="207"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="212"/>
        <source>Preferences...</source>
        <translation>Настройки...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="220"/>
        <source>New Pascal program</source>
        <translation>Новая Паскаль-программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="223"/>
        <source>Ctrl+P</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.ui" line="228"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1790"/>
        <source>Restore previous session</source>
        <translation>Восстановить предыдущий сеанс</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="162"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="174"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="186"/>
        <source>No actions for this tab</source>
        <translation>Для этой вкладки нет действий</translation>
    </message>
    <message>
        <source>No errors</source>
        <translation type="obsolete">Ошибок нет</translation>
    </message>
    <message>
        <source>Errors: %1</source>
        <translation type="obsolete">Ошибок: %1</translation>
    </message>
    <message>
        <source>Kumir programs (*.kum)</source>
        <translation type="obsolete">Программы Кумир (*.kum)</translation>
    </message>
    <message>
        <source>Pascal programs (*.pas *.pp</source>
        <translation type="obsolete">Программы Паскаль (*.pas *.pp)</translation>
    </message>
    <message>
        <source>Steps done: %1</source>
        <translation type="obsolete">Выполнено шагов: %1</translation>
    </message>
    <message>
        <source>New Program</source>
        <translation type="obsolete">Новая программа</translation>
    </message>
    <message>
        <source>New Text</source>
        <translation type="obsolete">Новый текст</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2015"/>
        <source>%1 programs (*%2)</source>
        <translation>Программы %1 (*%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1287"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2018"/>
        <source>Text files (*.txt)</source>
        <translation>Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="323"/>
        <source>Configure menu items...</source>
        <translation>Настроить элементы меню...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1177"/>
        <source>Please wait...</source>
        <translation>Подождите...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1178"/>
        <source>Executable file generation in progress.</source>
        <translation>Создается выполняемый файл.</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1182"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1230"/>
        <source>Native executables (*.%1)</source>
        <translation>Программы (*.%1)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1231"/>
        <source>Save native executable</source>
        <translation>Сохранить выполняемый файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1284"/>
        <source>%1 programs (*.%2)</source>
        <translation>Программы %1 (*.%2)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1289"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2020"/>
        <source>All files (*)</source>
        <translation>Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1290"/>
        <source>Save file</source>
        <translation>Сохранить файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1316"/>
        <source>Can&apos;t save file</source>
        <translation>Не могу сохранить файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1361"/>
        <source>Close editor</source>
        <translation>Закрытие текста</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1362"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1981"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2170"/>
        <source>Save current text?</source>
        <translation>В этом файле были проведены несохранённые изменения. При закрытии эти изменения будут потеряны.
Сохранить их?</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1369"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1860"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1988"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2177"/>
        <source>Don&apos;t save</source>
        <translation>Не сохранять</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1371"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1862"/>
        <source>Cancel closing</source>
        <translation>Отменить закрытие</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1415"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1452"/>
        <source>%1 language settings</source>
        <translation>Настройки языка %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1453"/>
        <source>Runtime settings</source>
        <translation>Настройки выполнения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1455"/>
        <source>General settings</source>
        <translation>Общие настройки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1461"/>
        <source>Actor settings</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1791"/>
        <source>Are you sure to restore previous session? All unsaved changes will be lost.</source>
        <translation>Вы уверены, что хотите восстановить предыдущий сеанс? Все открытые файлы будут закрыты без сохранения.</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1795"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1796"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1849"/>
        <source>The following files have changes:
%1
Save them?</source>
        <translation>Эти файлы были изменены:
%1
Сохранить их?</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1852"/>
        <source>Close Kumir</source>
        <translation>Выход из Кумир</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1980"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2169"/>
        <source>Open another file</source>
        <translation>Открытие другого файла</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="1990"/>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2179"/>
        <source>Cancel opening another file</source>
        <translation>Не открывать другой файл</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2017"/>
        <source>Web pages (*.html *.htm)</source>
        <translation>Web-страницы (*.html *.htm)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2239"/>
        <source>Can&apos;t open file</source>
        <translation>Не могу открыть файл</translation>
    </message>
    <message>
        <source>Before creating new program:</source>
        <translation type="obsolete">Перед созданием новой программы:</translation>
    </message>
    <message>
        <source>Before closing tab:</source>
        <translation type="obsolete">Перед закрытием вкладки:</translation>
    </message>
    <message>
        <source>Cancel &quot;New program&quot;</source>
        <translation type="obsolete">Отменить создание новой программы</translation>
    </message>
    <message>
        <source>Cancel tab close</source>
        <translation type="obsolete">Отменить закрытие вкладки</translation>
    </message>
    <message>
        <source>Close without saving</source>
        <translation type="obsolete">Закрыть без сохранения</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Начало</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="obsolete">О программе</translation>
    </message>
    <message>
        <source>Kumir version %1
Using Qt version %2</source>
        <translation type="obsolete">Кумир версия %1
Использует Qt версии %2</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/mainwindow.cpp" line="2024"/>
        <source>Load file...</source>
        <translation>Загрузить файл...</translation>
    </message>
    <message>
        <source>Kumir programs (*.kum);;Pascal programs (*.pas *.pp);;Web pages (*.html *.htm);;Text files (*.txt);;All files (*)</source>
        <translation type="obsolete">Программы Кумир (*.kum);;Программы Pascal (*.pas *.pp);; Web-страницы (*.html *.htm);;Текстовые файлы (*.txt);;Все файлы (*)</translation>
    </message>
</context>
<context>
    <name>CoreGUI::PascalProgram</name>
    <message>
        <source>Stop</source>
        <translation type="obsolete">Остановить выполнение</translation>
    </message>
    <message>
        <source>Blind run</source>
        <translation type="obsolete">Без показа на полях и обновления окна величин</translation>
    </message>
</context>
<context>
    <name>CoreGUI::Plugin</name>
    <message>
        <source>Input/Output terminal</source>
        <translation type="obsolete">Область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="600"/>
        <source>Variables</source>
        <translation>Значения величин</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="543"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="554"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="220"/>
        <source>Remote Control</source>
        <translation>Пульт</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="80"/>
        <source>PROGRAM.kum</source>
        <translation>ПРОГРАММА.kum</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="81"/>
        <source>Source file name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="190"/>
        <source>Input/Output</source>
        <translation>Ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="236"/>
        <source>Save console output</source>
        <translation>Сохранить вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="256"/>
        <source>Copy to clipboard console output</source>
        <translation>Скопировать вывод в буфер обмена</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="370"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="400"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="410"/>
        <location filename="../../../src/plugins/coregui/guisettingspage.cpp" line="189"/>
        <source>Courses</source>
        <translation>Практикум</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="514"/>
        <source>Show actor window</source>
        <translation>Показать окно исполнителя</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="570"/>
        <source>Show actor control</source>
        <translation>Показать пульт</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="578"/>
        <source>Actor&apos;s References</source>
        <translation>Исполнители</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="909"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="910"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="941"/>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="950"/>
        <source>No actions for this tab</source>
        <translation>Для этой вкладки нет действий</translation>
    </message>
    <message>
        <source>Editing</source>
        <translation type="obsolete">Редактирование</translation>
    </message>
    <message>
        <source>Observe</source>
        <translation type="obsolete">Анализ</translation>
    </message>
    <message>
        <source>Running</source>
        <translation type="obsolete">Выполнение</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Пауза</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/plugin.cpp" line="916"/>
        <source>Start</source>
        <translation>Начало</translation>
    </message>
</context>
<context>
    <name>CoreGUI::StatusBar</name>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="144"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="493"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="497"/>
        <source>rus</source>
        <translation>рус</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="145"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="491"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="499"/>
        <source>lat</source>
        <translation>lat</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="160"/>
        <source>Row: ww, Col.: ww</source>
        <translation>Стр: ww, Кол: ww</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="331"/>
        <source>Edit</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="324"/>
        <source>Analisys</source>
        <translation>Анализ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <source>Run</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="169"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="321"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="184"/>
        <source>ww errors</source>
        <translation>ww ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="185"/>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="360"/>
        <source>No errors</source>
        <translation>Ошибок нет</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="186"/>
        <source>wwwww steps done</source>
        <translation>Выполнено wwwww шагов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="327"/>
        <source>Running</source>
        <translation>Выполнение</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="362"/>
        <source>1 error</source>
        <translation>1 ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="364"/>
        <source>%1 errors</source>
        <comment>10 &lt;= x &lt;= 20</comment>
        <translation>%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="366"/>
        <source>%1 errors</source>
        <comment>1, 21, 31, etc.</comment>
        <translation>%1 ошибка</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="368"/>
        <source>%1 errors</source>
        <comment>2, 3, 4, 22, 23, 24,  etc.</comment>
        <translation>%1 ошибки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="370"/>
        <source>%1 errors</source>
        <comment>5, 6, 15, 16, etc</comment>
        <translation>%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="378"/>
        <source>%1 steps done</source>
        <comment>10 &lt;= x &lt;= 20</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="380"/>
        <source>%1 steps done</source>
        <comment>1, 21, 31, etc.</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="382"/>
        <source>%1 steps done</source>
        <comment>2, 3, 4, 22, 23, 24,  etc.</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="384"/>
        <source>%1 steps done</source>
        <comment>5, 6, 15, 16, etc</comment>
        <translation>Выполнено шагов: %1</translation>
    </message>
    <message>
        <source>%1 errors</source>
        <translation type="obsolete">%1 ошибок</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="374"/>
        <source>0 steps done</source>
        <translation>Выполнено шагов: 1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="376"/>
        <source>1 step done</source>
        <translation>Выполнено шагов: 1</translation>
    </message>
    <message>
        <source>%1 steps done</source>
        <translation type="obsolete">Выполнено %1 шагов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/statusbar.cpp" line="459"/>
        <source>Row: %1, Column: %2</source>
        <translation>Стр: %1, Кол: %2</translation>
    </message>
</context>
<context>
    <name>CoreGUI::SwitchWorkspaceDialog</name>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="20"/>
        <source>Workspace Launcher</source>
        <translation>Выбор каталога пользователя</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Select a workspace&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Kumir stores your programs, data and settings in a folder called a workspace.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Choose a workspace folder to use for this session.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Выберете рабочий каталог&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-weight:600;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Кумир хранит Ваши программы, настройки и файлы данных в едином рабочем каталоге.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Выберете каталог, который будет использовать Кумир.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="47"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Droid Sans&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="60"/>
        <source>Workspace:</source>
        <translation>Каталог пользователя:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="80"/>
        <source>Browse...</source>
        <translation>Обзор...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.ui" line="89"/>
        <source>Use this as the default and do not ask again</source>
        <translation>Использовать этот каталог всегда и больше не спрашивать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.cpp" line="53"/>
        <source>&lt;p class=&apos;heading&apos;&gt;Choose working directory&lt;/p&gt;</source>
        <translation>&lt;p class=&apos;heading&apos;&gt;Рабочий каталог системы &quot;Кумир&quot;&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.cpp" line="54"/>
        <source>&lt;p class=&apos;main&apos;&gt;Working directory is a place to access files within your program using &lt;span class=&apos;code&apos;&gt;WORKING_DIRECTORY&lt;/span&gt; location.&lt;/p&gt;</source>
        <translation type="unfinished">&lt;p class=&apos;main&apos;&gt;Рабочий каталог - это каталог, в котором выполняется поиск файлов для ввода и вывода, если программа не сохранена. Из программы это значение можно получить алгоритмом &lt;span class=&apos;code&apos;&gt;РАБОЧИЙ&amp;nbsp;КАТАЛОГ&lt;/span&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.cpp" line="57"/>
        <source>&lt;p class=&apos;main&apos;&gt;Kumir also uses this directory to store your personal settings.&lt;p&gt;</source>
        <translation>&lt;p class=&apos;main&apos;&gt;Кроме того, в этом каталоге Кумир хранит Ваши персональные настройки.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/switchworkspacedialog.cpp" line="81"/>
        <source>Select directory for use as workspace</source>
        <translation>Выбор рабочего каталога</translation>
    </message>
</context>
<context>
    <name>CoreGUI::SystemOpenFileSettings</name>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.ui" line="14"/>
        <source>File Open</source>
        <translation>Открытие файлов</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.ui" line="20"/>
        <source>Open .kum file by system in:</source>
        <translation>Открывать файлы &quot;*.kum&quot; в:</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/systemopenfilesettings.cpp" line="26"/>
        <source>Choose on run</source>
        <translation>Спросить при запуске</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabBar</name>
    <message>
        <location filename="../../../src/plugins/coregui/tabbar.cpp" line="176"/>
        <location filename="../../../src/plugins/coregui/tabbar.cpp" line="178"/>
        <source>&lt;b&gt;Ctrl+%1&lt;/b&gt; activates this tab</source>
        <translation>&lt;b&gt;Ctrl+%1&lt;/b&gt; переключает на эту вкладку</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabWidget</name>
    <message>
        <source>Close current tab</source>
        <translation type="obsolete">Закрыть текущую вкладку</translation>
    </message>
</context>
<context>
    <name>CoreGUI::TabWidgetElement</name>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="230"/>
        <source>%1 (Course)</source>
        <translation>%1 (Практикум)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="247"/>
        <source>New Program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/tabwidgetelement.cpp" line="250"/>
        <source>New Text</source>
        <translation>Новый текст</translation>
    </message>
</context>
<context>
    <name>CoreGUI::ToolbarContextMenu</name>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="35"/>
        <source>Customize tool bar icons</source>
        <translation>Настроить отображаемые значки</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="87"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="88"/>
        <source>Reset to default</source>
        <translation>Сброк настроек отображения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/toolbarcontextmenu.cpp" line="89"/>
        <source>Check all</source>
        <translation>Показывать все</translation>
    </message>
</context>
<context>
    <name>Terminal::OneSession</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="750"/>
        <source>&gt;&gt; %1:%2:%3 - %4 - Process started</source>
        <translation>&gt;&gt; %1:%2:%3 - %4 - Начало выполнения</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="760"/>
        <source>&gt;&gt; %1:%2:%3 - %4 - Process finished</source>
        <translation>&gt;&gt; %1:%2:%3 - %4 - Выполнение завершено</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="816"/>
        <source>INPUT raw data to console stream</source>
        <translation>Ввод данных в файл &quot;консоль&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="819"/>
        <source>INPUT </source>
        <translation>ВВОД </translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="826"/>
        <source>string</source>
        <translation>лит</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="828"/>
        <source>integer</source>
        <translation>цел</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="830"/>
        <source>real</source>
        <translation>вещ</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="832"/>
        <source>charect</source>
        <translation>сим</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="834"/>
        <source>boolean</source>
        <translation>лог</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="997"/>
        <source>INPUT ERROR: Not a &apos;%1&apos; value</source>
        <translation>ОШИБКА ВВОДА: Это не значение &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="1006"/>
        <source>INPUT ERROR: Extra input</source>
        <translation>ОШИБКА ВВОДА: Введено лишнее</translation>
    </message>
    <message>
        <source>Not a &apos;%1&apos; value</source>
        <translation type="obsolete">Это не значение типа %1</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_onesession.cpp" line="1050"/>
        <source>RUNTIME ERROR: %1</source>
        <translation>ОШИБКА ВЫПОЛНЕНИЯ: %1</translation>
    </message>
</context>
<context>
    <name>Terminal::Plane</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_plane.cpp" line="27"/>
        <source>Copy to clipboard</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal_plane.cpp" line="31"/>
        <source>Paste from clipboard</source>
        <translation>Вставить</translation>
    </message>
</context>
<context>
    <name>Terminal::Term</name>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="17"/>
        <source>Input/Output</source>
        <translation>Ввод/вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="55"/>
        <source>Save last output</source>
        <translation>Сохранить последний вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="59"/>
        <source>Copy last output</source>
        <translation>Скопировать последний вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="63"/>
        <source>Copy all output</source>
        <translation>Скопировать весь вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="67"/>
        <source>Open last output in editor</source>
        <translation>Открыть последний вывод в редакторе</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="72"/>
        <source>Save all output</source>
        <translation>Сохранить весь вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="76"/>
        <source>Clear output</source>
        <translation>Очистить область ввода/вывода</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="258"/>
        <source>New Program</source>
        <translation>Новая программа</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="429"/>
        <source>Save output...</source>
        <translation>Сохранить вывод в файл...</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="431"/>
        <source>Text files (*.txt);;All files (*)</source>
        <translation>Текстовые файлы (*.txt);;Все файлы (*)</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="443"/>
        <source>Can&apos;t save output</source>
        <translation>Невозможно сохранить вывод</translation>
    </message>
    <message>
        <location filename="../../../src/plugins/coregui/terminal.cpp" line="444"/>
        <source>The file you selected can not be written</source>
        <translation>Выбранный Вами файл не может быть создан или перезаписан</translation>
    </message>
</context>
<context>
    <name>Terminal::Terminal</name>
    <message>
        <source>Save last output</source>
        <translation type="obsolete">Сохранить последний вывод</translation>
    </message>
    <message>
        <source>Open last output in editor</source>
        <translation type="obsolete">Открыть последний вывод в редакторе</translation>
    </message>
    <message>
        <source>Save all output</source>
        <translation type="obsolete">Сохранить весь вывод</translation>
    </message>
    <message>
        <source>Clear output</source>
        <translation type="obsolete">Очистить область ввода/вывода</translation>
    </message>
</context>
</TS>
